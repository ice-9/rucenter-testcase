#!/usr/bin/env python3

import bgpdump
from aggregate_prefixes import aggregate_prefixes
import database

# filter full BGP table by RU-CENTER ASN (and keep only ipv4 prefixes for simplification)
prefixes_list = bgpdump.get_prefixes()

# aggregate prefixes
prefixes_aggregated = list(aggregate_prefixes(prefixes_list))
print('\n',"RU-CENTER aggregated prefixes:")
print('\n'.join(map(str, prefixes_aggregated)))

# put aggregated prefixes to database
database.write_list(prefixes_aggregated)
