#!/usr/bin/env python3

import mysql.connector
from database_config import credentials as secrets

db_host = secrets['mysql']['host']
db_username = secrets['mysql']['username']
db_password = secrets['mysql']['password']
db_name = secrets['mysql']['database']

def write_list(l):
    conn = mysql.connector.connect(host=db_host, user=db_username, passwd=db_password, database=db_name)
    cursor = conn.cursor()
    sql = "INSERT INTO prefixes (prefix) VALUES (%s)"
    for i in l:
        cursor.execute(sql, (str(i),))

    conn.commit()
    cursor.close()
    conn.close()
