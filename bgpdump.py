#!/usr/bin/env python3

from bgpdumpy import BGPDump, TableDumpV2

def get_prefixes():

    # RU-CENTER ASN from https://bgpview.io/search/RU-CENTER#results-asns
    asn = ['25537', '39494', '48287', '5537']
    prefixes = []

    print('RU-CENTER prefixes -> ASN:')
    with BGPDump('rib.20220129.1000.bz2') as bgp:
        for entry in bgp:

            # entry.body can be either be TableDumpV1 or TableDumpV2
            if not isinstance(entry.body, TableDumpV2):
                continue  # I expect an MRT v2 table dump file

            # get a string representation of this prefix
            prefix = '%s/%d' % (entry.body.prefix, entry.body.prefixLength)

            # get a list of each unique originating ASN for this prefix
            originatingASs = set([
                route.attr.asPath.split()[-1]
                for route
                in entry.body.routeEntries])

            # get address family
            address_family = '%s' % entry.body.afi

            # filter prefixes by company ASN and filter only ipv4 for simplification
            if ((list(originatingASs)[0]) in asn and
                    address_family != "AddressFamily.AF_INET6"):
                print('%s -> %s' % (prefix, '/'.join(originatingASs)))
                prefixes.append(prefix)

    return(prefixes)

